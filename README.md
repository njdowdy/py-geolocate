# py-geolocate

## What is this?

This repo packages the `geolocate` Python script inside of a Docker container.

The `geolocate` script does the following:

1. Analyze CSV files of specimen observation data
2. Extract relevant columnar data containing locality information
3. Parse locality info into a valid URL for the Geolocate webservice
4. Execute an HTTP GET request for the constructed URL to retrieve the georeferenced data
5. Write the georeference data to a new CSV file

See [Advanced Use](#advanced-use) for more details on the scripts themselves.

## Why did we package this in a Docker container?

Packaging in Docker makes it easier to ensure that all of the files are in the proper locations and that the Python environment is the correct version (Python 3.7).

You can be more confident that any issues that arise are due to how the script is being used or how the data are formatted, and not because of an error in how the environment in which it is being run is set up. This is helpful for making things more reproducible between computing environments and easier to troubleshoot.

## Getting started

To make it easy for you to get started, here's how to get the analysis running.

## Pre-Requisites

- Ensure you have both Docker and Docker Compose installed on your local machine. See: [Install Docker Desktop](https://docs.docker.com/desktop/release-notes/)
- Ensure you have git installed on your local machine. See: [Install Git](https://git-scm.com/downloads)

## Clone this repository to you local machine

Open a terminal and navigate to the parent folder you want to clone the repository to. Then run the following command:

```
git clone https://gitlab.com/njdowdy/py-geolocate.git
cd py-geolocate
```

## Add your data to process

Files to process should be CSVs in UTF-8 format and should be placed in the `files/input` directory. It is important that the files do not have BOM encoding. You can remove this with a Python script like this:

```python
    import codecs
    with codecs.open('path/to/file/BOM_encoded.csv', 'r', encoding='utf-8-sig') as f:
        content = f.read()
    with codecs.open('path/to/file/Not_BOM_encoded.csv', 'w', encoding='utf-8') as f:
        f.write(content)
```

## Modify the `files/script/script.sh` file

The `script.sh` file is the entrypoint for the Docker container. It is a shell script that runs the `geolocate` script using the specified parameters. You will need to modify the the following `geolocate` parameters within the `script.sh` file (located in the `script` folder) to match your needs:

- the `-i` parameter should point to the input file you want to process
- the `-o` parameter should point to the output file you want to send the geolocated data to
- the `-l` parameter should identify the columns in your CSV file you want to use to georeference the data using a comma-separated list. The order of these is important and determines the order in which they will be used to georeference the data.

Ensure that the column names you provide in the `-l` parameter match the formatting and spelling in the header row of your CSV file. These also need to match exactly to what `geolocate` expects so that the script can map the columns to the correct fields in the Geolocate webservice. "Country" will not match "country" for example. Those fields are:

- `country`
- `locality`
- `stateProvince`
- `county`

## Running the Docker Container

With all your files in place and the `script.sh` file modified, you can now run the Docker container. Open a terminal and navigate to the root of the repository where the `docker-compose.yml` file is located. Then run the following command:

```
docker-compose up
```

If you receive an message about a "Cannot connect to the Docker daemon", you likely need to start the Docker Desktop application on your local machine.

To stop the container, you can press `Ctrl+C` in the terminal window where the container is running or you can run the following command:

```
docker-compose down
```

## Process Your Files

Currently, the script will not begin processing the input files until you manually run it from within the Docker container. This is to ensure that you are ready to begin processing and to avoid accidentally overwriting your output files.

To begin processing, start the container as outlined [above](#running-the-docker-container) and execute the following:

```
docker exec -ti py-geolocate /bin/bash
```

You should now be in a new terminal within the context of the Docker container (e.g., type `ls` to see a list of files). Now you can run the `script.sh` file to begin processing your input files using the following command:

```
/data/script/script.sh
```

Note, the `script.sh` is now located in the `/data/script` directory within the Docker container rather than `/files/script`. This is because the Docker container mounts the `files` directory to the `/data` directory within the container.

## Special notes

This script can take a long time to process, depending on how many records you have. This is because HTTP requests must be made for each observation and the speed depends on network latency, the performance of the Geolocate web service, etc.

If an error arises due to network communications, you will need to:

1. Examine which observation records completed successfully and remove those from the input file so they will not be processed again
2. Point the `script.sh` to a new output file to avoid overwritting your successfully georeferenced records (e.g., to "data/output_part2.csv")
3. Re-run the script to begin re-processing records

This is a time consuming and error-prone process. Thus, we recommend you only process a manageable number of records at a time and watch out for this possibility before producing your final output dataset.

Improving error handling and retries would be a welcomed addition to the `geolocate.py` script.

## Advanced Use

- You can find the `geolocate.py` script in `files/geolocate.py` to see how Geolocate is actually working
- You can find the `script.sh` file in `files/script/script.sh` to see how the `geolocate.py` script is being called and to modify the inputs, outputs, and columns to be used for georeferencing. You can also add other parameters that Geolocate understands if needed.
- You can setup the container to run the `geolocate.py` script on start up by modifying the `docker-compose.yml` file. This would allow you to run the script automatically upon Docker container start up, without needing to enter the container and run the `script.sh` file manually.

## Contributing

If you find an problem or have a question, please open an issue in the repository. If you would like to contribute, please open a merge request.

## Authors and acknowledgment

This script was packaged in a Docker container by [Nick Dowdy](https://gitlab.com/njdowdy). The `geolocate` script was written by [Nelson Rios](https://github.com/nelsonrios) and can be found [here](https://github.com/YPM-Informatics/glc_py/tree/master)

## License

This project is licensed under the MIT License - see the LICENSE.md file for details.
