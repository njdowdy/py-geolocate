# you will need to make this .sh file executable by running `chmod +x /data/script.sh` from within the Docker container
# you can then run the script by running `/data/script.sh` from within the Docker container
# feel free to modify the input (-i) and output (-o) paths as needed as well as the -l parameter to match the columns you want to geolocate
python3 /data/script/geolocate.py -i /data/input/Events_to_be_georeferenced_1_15_24_UTF8_clean.csv -o /data/output/output-2024-01-19.csv -l locality,municipality,county,island
